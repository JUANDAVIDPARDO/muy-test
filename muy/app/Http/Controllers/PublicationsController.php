<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;

class PublicationsController extends Controller
{
    /** 
    * @return JSON 
    */
    public function show($id)
    {
        // Initialize Firebase database
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/muy-test-69e3a772ce78.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://muy-test.firebaseio.com')
        ->create();

        $database = $firebase->getDatabase();

        return response()->json(
            $database->getReference("blog/posts/$id")
            // order the reference's children by their key in ascending order
            ->getSnapshot()->getValue()
        );
    }

    /** 
    * @return JSON 
    */
    public function showAll()
    {
        // Initialize Firebase database
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/muy-test-69e3a772ce78.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://muy-test.firebaseio.com')
        ->create();

        $database = $firebase->getDatabase();

        return response()->json(
            $database->getReference('blog/posts')
            // order the reference's children by their key in ascending order
            ->getSnapshot()->getValue()
        );
    }

    /** 
    * @return JSON 
    */
    public function getLike($id)
    {
        // Initialize Firebase database
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/muy-test-69e3a772ce78.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://muy-test.firebaseio.com')
        ->create();

        $database = $firebase->getDatabase();

        return response()->json(
            $database->getReference("blog/posts/$id/likes")
            // order the reference's children by their key in ascending order
            ->getSnapshot()->getValue()
        );
    } 
    
    /** 
    * @return JSON 
    */
    public function getComments($id)
    {
        // Initialize Firebase database
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/muy-test-69e3a772ce78.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://muy-test.firebaseio.com')
        ->create();

        $database = $firebase->getDatabase();

        return response()->json(
            $database->getReference("blog/posts/$id/comments")
            // order the reference's children by their key in ascending order
            ->getSnapshot()->getValue()
        );
    } 

    /** 
    * @return JSON 
    */
    public function create(Request $request)
    {
        // Add the date to the document
        $data = $request->json()->get('publication');
        $data["date"] = date("Y-m-d H:i:s");

        // Initialize Firebase database
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/muy-test-69e3a772ce78.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://muy-test.firebaseio.com')
        ->create();

        $database = $firebase->getDatabase();
        
        // push data -> data = json + date
        $newPost = $database
        ->getReference('blog/posts')
        ->push($data);

        return response()->json([
            'response' => [
                'code' => 200,
                'message' => "successful",
            ]
        ]);
    }  
    
    /** 
    * @return JSON 
    */
    public function addLike(Request $request)
    {
        
        $publicationId = $request->json()->get('publicationId');
        $body = $request->json()->get('like');

        // Add the date to the document
        $body["date"] = date("Y-m-d H:i:s");

        // Initialize Firebase database
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/muy-test-69e3a772ce78.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://muy-test.firebaseio.com')
        ->create();

        $database = $firebase->getDatabase();
        
        // push data -> data = json + date
        $newPost = $database
        ->getReference("blog/posts/$publicationId/likes")
        ->push($body);

        return response()->json([
            'response' => [
                'code' => 200,
                'message' => "successful",
            ]
        ]);
    }      

    /** 
    * @return JSON 
    */
    public function addComment(Request $request)
    {
   
        $publicationId = $request->json()->get('publicationId');
        $body = $request->json()->get('comment');

        // Add the date to the document
        $body["date"] = date("Y-m-d H:i:s");

        // Initialize Firebase database
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/muy-test-69e3a772ce78.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://muy-test.firebaseio.com')
        ->create();

        $database = $firebase->getDatabase();
        
        // push data -> data = json + date
        $newPost = $database
        ->getReference("blog/posts/$publicationId/comments")
        ->push($body);

        return response()->json([
            'response' => [
                'code' => 200,
                'message' => "successful",
            ]
        ]);

    }      

}