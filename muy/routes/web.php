<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// auth
$router->post('auth/login', ['uses' => 'AuthController@authenticate']);


$router->group(
    ['middleware' => 'jwt.auth'], 
    function() use ($router) {
        // Show all publications
        $router->get('/publication', 'PublicationsController@showAll');

        // Show a specific publication
        $router->get('/publication/{id}', 'PublicationsController@show');

        // Show likes of the a specific publication
        $router->get('/publication/likes/{id}', 'PublicationsController@getLike');

        // Show comments of the a specific publication
        $router->get('/publication/comments/{id}', 'PublicationsController@getComments');

        // Create publication
        $router->post('/publication/create/', 'PublicationsController@create');

        // Add like a publication specific
        $router->post('/publication/addLike', 'PublicationsController@addLike');

        // Add comments a publication specific
        $router->post('/publication/addComment', 'PublicationsController@addComment');
    }
);